package com.example.topnews;

public class news {
    private String author;
    private String title;
    private String url;
    private String image;

    public news(String author, String title, String url, String image) {
        this.author = author;
        this.title = title;
        this.url = url;
        this.image = image;
    }

    public String getAuthor() {
        return author;
    }

    public String getTitle() {
        return title;
    }

    public String getUrl() {
        return url;
    }

    public String getImage() {
        return image;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public void setImage(String image) {
        this.image = image;
    }
}
